# C Notes

## class notes

### 5/6/20
* jvm for java
	* software == hardware
	* java compiles to java-byte code for java virtual machine
* c code runs on the machine so it is machine code
* computers only understand machine code
* people who made used ascii
* c use unicode now
* c is not strongly typed
* EOF is end of file

### 5/13/20
* `return 0` purpose is to return to its parent function
* `return 0` means exit success
* the os calls main
* when you declare a variable, they begin with a garbage
* no one knows what is inside garbage
* go as far you can get. send an email by monday night

### 5/20/20
* finish chapter 1

### 5/27/20
* array example? 

## perface and introduction
* developed by close to unix
* higher leveled mechanisms must be provided by explictly-called functions
* Standard I/O library
* "C retains the basic philosophy that programmers know what they are doing; it
only reqiures that they state their intentions explictly"

## chapter 1
* every program has a function named main because the execution starts with
main
* \somecharacter is used for special characters like newline and tab
* integer division truncates (fractional parts are left out)
* use symbolic constants to represent constants
```
	for (initialization; conditional; increment)
		printf("format", first %, second %);
```
* symbolic constants are not variables
* EOF's value is -1
* A `long` is at least 32 bits
* integers are sometimes only 16 bits
* exercise 1-11: I would test the program by giving it a variety of words.
words such as don't, full-time, one, 1

# unix haters

## preface
* "being small and simple is more important than being complete and correct"
