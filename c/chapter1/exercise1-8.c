#include <stdio.h>

int main()
{
	int c, n1, t1, s1;

	n1 = 0;
	t1 = 0; /* these must be zero because they start off as garbage */
	s1 = 0; /* this one did not start off as garbage for some reason */
	while ((c = getchar()) != EOF)
		if (c == '\n') /* newlines */
			++n1;
		else if (c == '\t') /* tabs */
			++t1;
		else if (c == ' ') /* blanks */
			++s1;

	printf("%d\n%d\n%d\n", n1, t1, s1);
}
