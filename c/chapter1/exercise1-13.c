#include <stdio.h>

#define IN 	1	/* inside a word */ 
#define OUT	0 	/* outside a word */

int main()
{
	int c, i, count, state;
	int ndigit[26];

	for (i = 0; i < 26; ++i)
		ndigit[i] = ' ';
		
	state = OUT;
	while ((c = getchar()) != EOF) {
		if (c == ' ' || c == '\n' || c == '\t') {
			state = OUT;
			putchar('\n');
		}
		else if (state == OUT) {
			state = IN;
		}
		else {
			
		}
	}
}
