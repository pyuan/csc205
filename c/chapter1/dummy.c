#include <stdio.h>

int main()
{
	int c, b1;
	b1 = 0;

	while ((c = getchar()) != EOF)
		if (c == ' ') 
			putchar(' ');
		else
			putchar(c);
	
	return 0;
}
