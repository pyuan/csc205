#include <stdio.h>

#define LOWER 0
#define UPPER 300
#define STEP 20

int convert(int m);

int main()
{
	int celsius;

	for (celsius = LOWER; celsius <= UPPER; celsius = celsius + STEP)
		printf("%3d %6.1d\n", celsius, convert(celsius));
	return 0;
}

int convert(int celsius)
{
	return(((9.0/5.0)*celsius)+32);
}
