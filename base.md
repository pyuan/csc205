| Base 10 | Base 2 | Base 5 | Base 8 | Base 16 | Base 20 |
| ------- | ------ | ------ | ------ | ------- | ------- |
| 0       | 0      | 0      | 0      | 0       | 0       |
| 1       | 1      | 1      | 1      | 1       | 1       |
| 2       | 10     | 2      | 2      | 2       | 2       |
| 3       | 11     | 3      | 3      | 3       | 3       |
| 4       | 100    | 4      | 4      | 4       | 4       |
| 5       | 101    | 10     | 5      | 5       | 5       |
| 6       | 110    | 11     | 6      | 6       | 6       |
| 7       | 111    | 12     | 7      | 7       | 7       |
| 8       | 1000   | 13     | 10     | 8       | 8       |
| 9       | 1001   | 14     | 11     | 9       | 9       |
| 10      | 1010   | 20     | 12     | A       | A       |
| 11      | 1011   | 21     | 13     | B       | B       |
| 12      | 1100   | 22     | 14     | C       | C       |
| 13      | 1101   | 23     | 15     | D       | D       |
| 14      | 1110   | 24     | 16     | E       | E       |
| 15      | 1111   | 30     | 17     | F       | F       |
| 16      | 10000  | 31     | 20     | 10      | G       |
| 17      | 10001  | 32     | 21     | 11      | H       |
| 18      | 10010  | 33     | 22     | 12      | I       |
| 19      | 10011  | 34     | 23     | 13      | J       |
| 20      | 10100  | 40     | 24     | 14      | 10      |
| 21      | 10101  | 41     | 25     | 15      | 11      |
| 22      | 10110  | 42     | 26     | 16      | 12      |
| 23      | 10111  | 43     | 27     | 17      | 13      |
| 24      | 11000  | 44     | 30     | 18      | 14      |
| 25      | 11001  | 100    | 31     | 19      | 15      |
| 26      | 11010  | 101    | 32     | 1A      | 16      |
| 27      | 11011  | 102    | 33     | 1B      | 17      |
| 28      | 11100  | 103    | 34     | 1C      | 18      |
| 29      | 11101  | 104    | 35     | 1D      | 19      |
| 30      | 11110  | 110    | 36     | 1E      | 1A      |
| 31      | 11111  | 111    | 37     | 1F      | 1B      |
| 32      | 100000 | 112    | 40     | 20      | 1C      |
| 33      | 100001 | 113    | 41     | 21      | 1D      |
| 34      | 100010 | 114    | 42     | 22      | 1E      |
| 35      | 100011 | 120    | 43     | 23      | 1F      |
| 36      | 100100 | 121    | 44     | 24      | 1G      |
| 37      | 100101 | 122    | 45     | 25      | 1H      |
| 38      | 100110 | 123    | 46     | 26      | 1I      |
| 39      | 100111 | 124    | 47     | 27      | 1J      |
| 40      | 101000 | 130    | 50     | 28      | 20      |
| 41      | 101001 | 131    | 51     | 29      | 21      |
| 42      | 101010 | 132    | 52     | 2A      | 22      |
| 43      | 101011 | 133    | 53     | 2B      | 23      |
| 44      | 101100 | 134    | 54     | 2C      | 24      |
| 45      | 101101 | 140    | 55     | 2D      | 25      |
| 46      | 101110 | 141    | 56     | 2E      | 26      |
| 47      | 101111 | 142    | 57     | 2F      | 27      |
| 48      | 110000 | 143    | 60     | 30      | 28      |
| 49      | 110001 | 144    | 61     | 31      | 29      |
| 50      | 110010 | 200    | 62     | 32      | 2A      |
